package excelFiles;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by jan_w on 20.01.2018.
 */
public class ExcelFileWriter {

    // TODO: convert to https://www.mkyong.com/java/apache-poi-reading-and-writing-excel-file-in-java/

    public void excelWriter(String sheetName, String excelFilePath, Object[][] dataToWrite) {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(sheetName);

        System.out.println("Creating Excel file started.");

        int rowNumber = 0;
        for (Object[] datatype: dataToWrite) {
            Row row = sheet.createRow(rowNumber++);
            int colNumber = 0;
            for (Object field: datatype) {
                Cell cell = row.createCell(colNumber++);
                if (field instanceof String){
                    cell.setCellValue((String) field);
                }else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                }
            }
        }

        try (FileOutputStream fos = new FileOutputStream(excelFilePath)){

            workbook.write(fos);
            fos.flush();
            fos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Done.");
    }
}
