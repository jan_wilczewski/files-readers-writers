package excelFiles;

/**
 * Created by jan_w on 24.01.2018.
 */
public class TestObjectToWrite {

    public Object[][] datatypes = {
            {"Datatype", "Type", "Size(in bytes)"},
            {"int", "Primitive", 2},
            {"float", "Primitive", 4},
            {"double", "Primitive", 8},
            {"char", "Primitive", 1},
            {"String", "Non-Primitive", "No fixed size"}
    };

    public Object[][] getDatatypes() {
        return datatypes;
    }
}
