package textFiles;

import java.io.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FileUtils;




/**
 * Created by jan_w on 19.01.2018.
 */
public class TextFileReader {

    private static int counter = 0;

    public void readTextFile (String fileName) {

        File fileToBeRead = new File(fileName);

        try (BufferedReader reader = new BufferedReader( new FileReader(fileToBeRead))) {
            String readText = null;
            while ((readText = reader.readLine()) != null){
                String[] receivedText = readText.split("/n");
                for (String text: receivedText){
                    System.out.println( text);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void searchWordInText(String fileName, String word) {
        int counter = 0;
        try {
            String text = FileUtils.readFileToString(new File(fileName));
            text = text.toLowerCase();
            counter = StringUtils.countMatches(text, word);

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (counter > 0) {
            System.out.println("Searched word: " + word + " was found in text " + counter + " times.");
        }
        else {
            System.out.println("There is no such word in this text.");
        }
    }
}
