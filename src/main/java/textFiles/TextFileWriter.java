package textFiles;

import java.io.*;
import java.util.Scanner;

/**
 * Created by jan_w on 19.01.2018.
 */
public class TextFileWriter {

    Scanner scanner = new Scanner(System.in);

    public void writeTextFile() {

        System.out.println("Please type name of the new file: ");
        String fileName = scanner.nextLine();

        File newFile = new File(fileName);

        try (FileOutputStream fos = new FileOutputStream(newFile);
             PrintWriter writer = new PrintWriter(fos)){

            String textLine = "";
            while (!textLine.equals("end")){

                System.out.println("Type text: ");
                textLine = scanner.nextLine();
                if (!textLine.equals("end")){
                    writer.println(textLine);
                    writer.flush();
                    System.out.println("Written text: " + textLine);
                }else{
                    System.out.println("End of the program.");
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
