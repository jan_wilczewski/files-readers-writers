package xmlFiles;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Map;


@XmlRootElement(name = "bookstore")
public class Bookstore {

    private List<Book> booksList;

    public Bookstore() {
    }

    public List<Book> getBooksList() {
        return booksList;
    }

    @XmlElement(name = "book")
    public void setBooksList(List<Book> booksList) {
        this.booksList = booksList;
    }

    @Override
    public String toString() {
        return "Bookstore{" +
                "booksList=" + booksList +
                '}';
    }
}
