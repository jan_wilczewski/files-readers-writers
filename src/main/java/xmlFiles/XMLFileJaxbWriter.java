package xmlFiles;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

/**
 * Created by jan_w on 22.01.2018.
 */
public class XMLFileJaxbWriter {

    public void xmlJaxbWriter(String filePath, Book book) {

        try {
            File file = new File(filePath);
            JAXBContext jaxbContext = JAXBContext.newInstance(Book.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(book, file);
            jaxbMarshaller.marshal(book, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
