package xmlFiles;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * Created by jan_w on 21.01.2018.
 */
public class XmlFileDomReader {

    public void basicXmlReader(String filePath) {

        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new File(filePath));

            // normalize text representation

            doc.getDocumentElement().normalize();
            System.out.println("Root element of the doc is " + doc.getDocumentElement().getNodeName());

            NodeList listOfBooks = doc.getElementsByTagName("book");
            int totalBooks = listOfBooks.getLength();
            System.out.println("Total number of books: " + totalBooks);
            System.out.println();

            for (int s = 0; s < listOfBooks.getLength(); s++) {

                Node bookNode = listOfBooks.item(s);
                if (bookNode.getNodeType() == Node.ELEMENT_NODE){

                    Element bookElement = (Element)bookNode;

                    NodeList titleList = bookElement.getElementsByTagName("title");
                    Element titleElement = (Element)titleList.item(0);

                    NodeList textTitleList = titleElement.getChildNodes();
                    System.out.println("Title: " + ((Node)textTitleList.item(0)).getNodeValue().trim());

                    NodeList authorList = bookElement.getElementsByTagName("author");
                    Element authorElement = (Element)authorList.item(0);

                    NodeList textAuthorList = authorElement.getChildNodes();
                    System.out.println("Author: " + ((Node)textAuthorList.item(0)).getNodeValue().trim());

                    NodeList yearList = bookElement.getElementsByTagName("year");
                    Element yearElement = (Element)yearList.item(0);

                    NodeList textYearList = yearElement.getChildNodes();
                    System.out.println("Year : " + ((Node)textYearList.item(0)).getNodeValue().trim());

                    NodeList priceList = bookElement.getElementsByTagName("price");
                    Element priceElement = (Element)priceList.item(0);

                    NodeList textPriceList = priceElement.getChildNodes();
                    System.out.println("Price : " + ((Node)textPriceList.item(0)).getNodeValue().trim());

                    System.out.println("--------------------");
                }
            }

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }
}
