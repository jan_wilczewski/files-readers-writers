package xmlFiles;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class XmlFileJaxbReader {


    private Bookstore bookstore = null;

    public Bookstore readAllFile(String pathName) {
        try {

            File file = new File(pathName);

            JAXBContext jaxbContext = JAXBContext.newInstance(Bookstore.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            bookstore = (Bookstore) jaxbUnmarshaller.unmarshal(file);

        } catch (JAXBException e) {
            e.printStackTrace();
        }return bookstore;
    }

    public Book findBookByTitle(String pathName, String title){

        Bookstore bookstore = readAllFile(pathName);
        List<Book> booksList = bookstore.getBooksList();

        Book book = null;
        for (int i = 0; i < booksList.size() ; i++) {
            if (booksList.get(i).getTitle().equals(title)){
                book = booksList.get(i);
                break;
            }
        }
        if (book == null){
            System.out.println("There is no book with such title in this xml file.");
        }
        System.out.println(book);
        return book;
    }

    public List<Book> findBooksByAuthor(String pathName, String author) {

        Bookstore bookstore = readAllFile(pathName);
        List<Book> bookList = bookstore.getBooksList();

        Book book = null;
        List<Book> bookListByAuthor = new ArrayList<>();
        for (int i = 0; i <bookList.size() ; i++) {
            if (bookList.get(i).getAuthor().equals(author)){
                bookListByAuthor.add(bookList.get(i));
            }
        }
        if (bookListByAuthor.isEmpty()){
            System.out.println("There is no book with such author in this xml file.");
        }
        System.out.println(bookListByAuthor);
        return bookListByAuthor;
    }
}
